package net.tqft.resolve

case class uriDictionary(mappings: List[uriMapping])

case class uriMapping(s: String, o: List[String])
package net.tqft.resolve

import org.bowlerframework.controller.Controller
import org.bowlerframework.controller.FunctionNameConventionRoutes
import org.bowlerframework.Request
import net.tqft.toolkit.amazon.S3
import net.tqft.toolkit.collections.MapCaching.map2CachableMap
import net.tqft.toolkit.collections.MapTransformer.valuesTransformable
import net.tqft.toolkit.Logging

class URIResController extends Controller with FunctionNameConventionRoutes with Logging { metaphorController =>

  object URN {
    def unapply(urn: String): Option[String] = {
      val pieces = urn.split(":").toList
      if (pieces.head.toLowerCase != "urn") {
        None
      } else {
        if (pieces.tail.size > 1) {
          Some("urn:" + pieces(1).toLowerCase() + pieces.drop(2).mkString(":"))
        } else {
          None
        }
      }
    }
  }

  val correspondences: scala.collection.mutable.Map[String, List[String]] = {
    import net.tqft.toolkit.collections.MapTransformer._
    import net.tqft.toolkit.collections.MapCaching._
    S3("article-correspondences")
      .transformValues({ s: String => s.split("\n").toList }, { ss: List[String] => ss.distinct.mkString("\n") })
      .caching
  }

  def `GET /uri-res/N2Ns`(request: Request): uriDictionary = {
    val lookup = request.getParameterNames.filter(_.toLowerCase().startsWith("urn:")).toList
    uriDictionary(lookup.map(s => uriMapping(s, correspondences.getOrElse(s, Nil))))
  }

  def `PUT /uri-res/N2Ns`(request: Request): uriDictionary = {
    val keys = request.getParameterNames.filter(_.startsWith("urn:"))
    for (
      key <- keys;
      value = request.getStringParameter(key).get;
      urn <- URN.unapply(value)
    ) {
      correspondences.put(key, urn :: correspondences.getOrElse(key, Nil))
    }

    `GET /uri-res/N2Ns`(request)
  }
}